#合作开发自动打包脚本。详见 https://wiki.n.miui.com/pages/viewpage.action?pageId=145101278

#获取即将打包的设备名称和tag
device_name=$1
tag_name=$2
current_branch=$3

if [[ $device_name = "" || tag_name = "" ]]; then
#statements
echo '[ERROR] 打包设备名称和tag号不能为空'
exit 0;
fi

#echo "开始"

cd ../mihomeinternal

git stash

git config --global https.proxy https://fq.mioffice.cn:3128
git config --global http.proxy http://fq.mioffice.cn:3128

# 切换到 base 分支
git checkout $current_branch
echo $device_name
new_branch_name="jenkins_${device_name}_${tag_name}"

echo $new_branch_name

# 删除旧分支
git branch -D $new_branch_name
git branch -r -d origin/$new_branch_name
git push origin :$new_branch_name
echo "push 完成"

# 切到新分支
git checkout -b $new_branch_name
echo "切分支完成"

git pull

#拷贝代码到internal/devices
cd ../MiHomePackageTool
echo "进package tool"

./device_configs.py -n $device_name -t $tag_name -b ""
result=$?
if [[ $result > 0 ]]; then
	echo "拷贝 $current_branch 分支的代码失败，请检查后重试。"
    exit 0;
fi

#提交

#加入新的代码
cd ../mihomeinternal
git status

git add .
echo "status"
git status
git commit -a -m "add $current_branch code"
#pod install。有时候可能有文件的增删，此处我们pod install一下
#rm -rf MiHome/Pods/Manifest.lock

cd MiHome 
pod _1.6.1_ install
if [[ $? != 0 ]]; then
    	#statements
    	echo "pod install error"
    	exit 1
	fi
#echo "pod install"
git add ..
git status
git commit -a -m "pod install"
#git commit -a -m "skip pod install"
#git push origin $current_branch
git push origin $new_branch_name

#Jenkins打包
echo "打包参数为：delay=0sec&token=zwf_cooperation_build&branch=$new_branch_name&tag=$tag_name&upload_fir=true&fir_token=ce8c5bc4174a562917c538fc704d90c1"
curl -X POST http://10.38.162.73/view/ios/job/ios-Feature/build \
--user tongchao1:1185dda70265c41842963ee2be25754886 \
--data-urlencode json='{"parameter": [{"name":"branch", "value":"'$new_branch_name'"}, {"name":"xcodeversion", "value":"xcode12"}, {"name":"tag", "value":"'$tag_name'"}, {"name":"upload_fir", "value":"true"}, {"name":"fir_token", "value":"ce8c5bc4174a562917c538fc704d90c1 [文锋]"}, {"name":"install", "value":"true"}]}'
#http_response=$(curl -s -o buildResponse.txt -w "%{http_code}" -X POST http://10.38.162.73/view/ios/job/ios-Feature/build --user tongchao1:1185dda70265c41842963ee2be25754886 --data-urlencode json='{"parameter": [{"name":"branch", "value":“jenkins_Xiaovv_1.2.8"}, {"name":"tag", "value":"1.2.8"}, {"name":"upload_fir", "value":"true"}, {"name":"fir_token", "value":"ce8c5bc4174a562917c538fc704d90c1 [文锋]"}, {"name":"xcodeversion", "value":"xcode12"}, {"name":"install", "value":"true"}]}')
#if [ $http_response != "200" ]; then
    # handle error
#else
#    echo "Server returned:"
#    cat buildResponse.txt   
#fi
echo "请打开 http://10.38.162.73/view/ios/job/ios-Feature/buildWithParameters 查看"
