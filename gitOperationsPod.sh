#device_name='MJCamera'
#tag='hlc6-6.110'
#base='release/0415'
device_name=$1
tag=$2
base=$3

cd ../mihomeinternal

git stash

# 切换到 base 分支
git checkout $base

echo $device_name
new_branch_name="jenkins_${device_name}_${tag}"

echo $new_branch_name

# 删除旧分支
git branch -D $new_branch_name
git branch -r -d origin/$new_branch_name
git push origin :$new_branch_name

# 切到新分支
git checkout -b $new_branch_name

git pull

# 根据 device 和 tag 生成新的 pod 指令
cd ../MiHomePackageTool
echo "进入"

pod_name="pod '$device_name'"
echo $pod_name

git_url=`python get_device_repo_url.py -n $device_name`

echo ${git_url}

pod_command="$pod_name, :git => '$git_url', :tag => '$tag'"

echo $pod_command


cd ../mihomeinternal
echo "进internal folder"

sed -i.backup "s?^    $pod_name.*?$pod_command?g" MiHome/SegmentPodfile/mihome_devices.rb
echo "pod name"
echo ${pod_name}
echo "pod command"
echo $pod_command

cd ../xmmodule

cd ./${device_name}

git pull
git push internal --tags

cd ..
cd ../mihomeinternal

# pod install 并提交代码到新分支
#cd MiHome
#pod update MiHomeFFmpeg

#pod _1.6.1_ repo update xiaomi-xmspec
#pod _1.6.1_ repo update xiaomi-xmspec-binary
#pod _1.6.1_ install
#if [[ $? != 0 ]]; then
        #statements
#       echo "pod install error"
#       exit 1
#       fi

#echo "pod install"

pwd
git status

git add .

git commit -a -m "skip pod install"

git push origin $new_branch_name
echo "install 结束 push"
# 调用 jenkins 的打包 API

curl -X POST http://10.38.162.73/view/ios/job/ios-Feature/build \
--user tongchao1:1185dda70265c41842963ee2be25754886 \
--data-urlencode json='{"parameter": [{"name":"branch", "value":"'$new_branch_name'"}, {"name":"tag", "value":"'$tag'"}, {"name":"upload_fir", "value":"true"}, {"name":"fir_token",
"value":"ce8c5bc4174a562917c538fc704d90c1 [文锋]"}, {"name":"xcodeversion", "value":"xcode12"}, {"name":"install", "value":"true"}]}'

echo "请打开 http://mjapp-jenks.miot.mi.srv/view/ios/job/ios-Feature/buildWithParameters 查看"



