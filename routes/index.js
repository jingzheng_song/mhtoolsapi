// <reference path="../types/jenkinsApi.d.ts"/>
const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');
const spawn = require('child_process').spawn;
const exec = require('child_process').exec;
const execSync = require('child_process').execFileSync;
const cors = require('cors');
const logger = require('morgan');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const jenkinsApi = require('jenkins-api');
const path = require('path');
const { stdout, stderr } = require('process');
const { Buffer } = require('buffer');

const history = new Set();
const jenkins = jenkinsApi.init("http://10.38.162.73");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/*--------------------------登录验证-------------------------------*/
router.post('/login', (req, res, next) => {
  history.clear();
  console.log("登录请求", req.body.params);
  const username = req.body.params.username;
  const password = req.body.params.password;

  if (username === 'mihome' && password === 'camera') {
    res.status(200).send({msg: "登录成功", token: "mjiosprivatetoken", tokenExpiration: "10000000"});
  } else {
    res.status(401).send({msg: "用户名与密码不匹配！"});
  }
})


/*--------------------------日志解密-------------------------------*/
router.get('/parse', (req, res, next) => {
  console.log("进入resources",req.query);
  const { fileName, filePath } = req.query;
    
  // const readStream = fs.createReadStream(`${__dirname}/../uploads/decrypted_${fileName}`);
  const readStream = fs.createReadStream(filePath);
  readStream.on('data', function (data) {
      res.write(data, 'binary');
  });

  exec(`rm -rf ${__dirname}/../uploads`, (err, stdout, stderr) => {
      if (err) {
          console.log('error:' + stderr);
      } else {
          console.log(stdout);
      }
  });
    
  readStream.on('end', function () {
      res.send();
  });
});

router.post('/upload', (req, res, next) => {
  console.log("req", req);
  try {
    if (!req.files) {
      return res.status(400).json({msg: 'No file uploaded'});
    } else {
      const file = req.files.file;
      const path = `${__dirname}/../uploads/${file.name}`;
      console.log("路径： " + path);

      file.mv(`${__dirname}/../uploads/${file.name}`, err => {
        if (err) {
          console.error(err);
          return res.status(500).send(err);
        }
      });

      let sdata = "";
      const sp = spawn("java", ['-jar', `${__dirname}/../iOSCommandTools.jar`, path]);
      sp.stdout.on('data', (data) => {
        sdata += data;
      });

      sp.on('close', (code) => {
        console.log(`stout: ${sdata}`);
        fs.writeFile(`${__dirname}/../uploads/decrypt_${file.name}`, sdata, err => {
          if (err) {
            console.error(err);
            return;
          }
          console.log(path);
          return res.json({ fileName: file.name, filePath: `${__dirname}/../uploads/decrypted_${file.name}`, ready: true/*`/uploads/${file.name}`*/});
        });
      });
    }
  } catch(err) {
    res.status(500).send(err);
  }
});

// 清除文件夹记录
router.get('/clear', (req, res, next) => {
  try {
    exec(`rm -rf ${__dirname}/../uploads`, (err, stdout, stderr) => {
      if (err) {
          console.log('error:' + stderr);
      } else {
          console.log(stdout);
      }
    });
    res.send("清除成功");
  } catch(err) {
    res.status(500).send(err);
  }
});


router.get('/mkdir', (req, res) => {
  exec(`mkdir ${__dirname}/../uploads`, (err, stdout, stderr) => {
      if (err) {
          console.log('error:' + stderr);
      } else {
          console.log(stdout);
      }
  });
  res.send();
});

/*--------------------------打包----------------------------*/

router.post('/pack', (req, res, next) => {
  const devices = ['ChuangMi', 'MIOWLDoorRing', 'ismartalarm', 'SimCamCamera', 'DunCateye', 'HTPrinter', 'Xiaovv', 'YDCatY'];
  const pods = ['MJCamera', 'DoorCamera', 'Lumi', 'XinAnVehicle', 'MGCamera', 'MXDevices', 'Router', 'MJCatEye'];

  console.log("req", req);

  const deviceName = req.body.device;
  console.log(deviceName);

  const base = req.body.base;
  console.log(base);

  const tag = req.body.tag;
  console.log(tag);

  if (devices.indexOf(deviceName) !== -1) {
    exec("chmod +x ./gitOperationsDevice.sh", (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });

    console.log("验证是device");
    exec(`./gitOperationsDevice.sh ${deviceName} ${tag} ${base}`, (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
  }

  if (pods.indexOf(deviceName) !== -1) {
    exec("chmod +x ./gitOperationsPod.sh", (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });

    exec(`./gitOperationsPod.sh ${deviceName} ${tag} ${base}`, (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
    console.log("验证是pod");
  }
  
  setTimeout(() => {
    return res.json({ready: true});
  }, 1000);
});

// abort任务
router.post('/stop', (req, res, next) => {
  // 前端传buildNumber
  const buildNumber = req.body.buildNumber;
  exec("chmod 755 ./stopJob.sh", (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });

  exec(`./stopJob.sh ${buildNumber}`, (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });
});

// 重新开始任务
router.post('/restart', (req, res, next) => {
  const devices = ['ChuangMi', 'MIOWLDoorRing', 'ismartalarm', 'SimCamCamera', 'DunCateye', 'HTPrinter', 'Xiaovv', 'YDCatY'];
  const pods = ['MJCamera', 'DoorCamera', 'Lumi', 'XinAnVehicle', 'MGCamera', 'MXDevices', 'Router', 'MJCatEye'];

  console.log("req", req);

  const deviceName = req.body.device;
  console.log(deviceName);

  const base = req.body.base;
  console.log(base);

  const tag = req.body.tag;
  console.log(tag);

  if (devices.indexOf(deviceName) !== -1) {
    exec("chmod +x ./gitOperationsDevice.sh", (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });

    console.log("验证是device");
    exec(`./gitOperationsDevice.sh ${deviceName} ${tag} ${base}`, (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
  }

  if (pods.indexOf(deviceName) !== -1) {
    exec("chmod +x ./gitOperationsPod.sh", (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });

    exec(`./gitOperationsPod.sh ${deviceName} ${tag} ${base}`, (err, stdout, stderr) => {
      if (err) {
        console.log('error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
    console.log("验证是pod");
  }
});

// 获取buildNumber
router.get('/getBuildNumber', (req, res, next) => {

  /*exec(`rm buildNumber.txt`, (err, stdout, stderr) => {
    if (err) {
        console.log('error:' + stderr);
    } else {
        console.log(stdout);
    }
  });*/

  exec("chmod 755 ./getBuildNumber.sh", (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });

  exec(`./getBuildNumber.sh`, (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });
  let buildNumber = "";

  fs.readFile('buildNumber.txt', (err, buffer) => {
  if (err) {
    console.log(err);
  } else {
    console.log(buffer.toString());
    buildNumber = parseInt(buffer.toString());
  }
});
  setTimeout(() => {
    return res.json({buildNumber});
  }, 2000);
});

// 根据buildNumber获取对应日志
router.get('/getInfo', (req, res, next) => {

  exec(`rm console.txt`, (err, stdout, stderr) => {
    if (err) {
        console.log('error:' + stderr);
    } else {
        console.log(stdout);
    }
  });

  const { buildNumber } = req.query;

  exec("chmod 755 ./getInfo.sh", (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });

  exec(`./getInfo.sh ${buildNumber}`, (err, stdout, stderr) => {
    if (err) {
      console.log('error:' + stderr);
    } else {
      console.log(stdout);
    }
  });
});

module.exports = router;
